<%--
  Created by IntelliJ IDEA.
  User: heli
  Date: 2015/8/28
  Time: 9:00
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <title></title>
</head>
<body>
<table id="main-role-manage-role-tb" fit="true"></table>
<div id="main-role-manage-add-dialog" title="添加角色" style="width:400px;height:170px;display: none;"
     data-options="iconCls:'icon-add',resizable:false,modal:true">

  <form method="post" id="main-role-manage-add-dialog-form">
    <table border="0" align="center">
      <tr>
        <td>角色名字</td>
        <td>
          <input class="easyui-textbox" style="width:200px;height: 30px;" name="name" data-options="required:true,validType:'roleName[1,30]',missingMessage:'请输入角色名字'">
        </td>
      </tr>
      <tr>
        <td>角色键值</td>
        <td>
          <input class="easyui-textbox" style="width:200px;height:30px;" name="key" data-options="required:true,validType:'roleKey[1,200]',missingMessage:'请输入角色键值'">
        </td>
      </tr>
    </table>
  </form>
</div>

<div id="main-role-manage-edit-dialog" title="编辑角色" style="width:400px;height:170px;display: none;"
     data-options="iconCls:'icon-edit',resizable:false,modal:true">

  <form method="post" id="main-role-manage-edit-dialog-form">
    <table border="0" align="center">
      <tr>
        <td>角色名字</td>
        <td>
          <input class="easyui-textbox" id="main-role-manage-edit-dialog-form_name" style="width:200px;height: 30px;" name="name" data-options="required:true,validType:'roleName[1,30]',missingMessage:'请输入角色名字'">
        </td>
      </tr>
      <tr>
        <td>角色键值</td>
        <td>
          <input class="easyui-textbox" id="main-role-manage-edit-dialog-form_key" style="width:200px;height:30px;" name="key" data-options="required:true,validType:'roleKey[1,200]',missingMessage:'请输入角色键值'">
        </td>
      </tr>
    </table>
  </form>
</div>
<div id="main-role-manage-check-permission-dialog" title="查看权限" style="width:300px;height:500px;display: none;"
     data-options="iconCls:'icon-ok',resizable:true,modal:true">
  <ul id="main-role-manage-check-permission-dialog-pers"></ul>
</div>
<script>
  seajs.use(['main/role/manage'],function(m){
    m.init('${pageContext.request.contextPath}');
  });
</script>
</body>
</html>
