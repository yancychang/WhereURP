package com.whereta.model;

import java.io.Serializable;

public class RolePermission implements Serializable {
    /**
     * role_permission.id
     * @ibatorgenerated 2015-08-27 16:17:57
     */
    private Integer id;

    /**
     * role_permission.role_id (角色id)
     * @ibatorgenerated 2015-08-27 16:17:57
     */
    private Integer roleId;

    /**
     * role_permission.permission_id (权限id)
     * @ibatorgenerated 2015-08-27 16:17:57
     */
    private Integer permissionId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public Integer getPermissionId() {
        return permissionId;
    }

    public void setPermissionId(Integer permissionId) {
        this.permissionId = permissionId;
    }
}