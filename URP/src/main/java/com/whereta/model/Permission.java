package com.whereta.model;

import java.io.Serializable;

public class Permission implements Serializable {
    /**
     * permission.id (权限id)
     * @ibatorgenerated 2015-09-01 10:10:36
     */
    private Integer id;

    /**
     * permission.name (权限名字)
     * @ibatorgenerated 2015-09-01 10:10:36
     */
    private String name;

    /**
     * permission.key (权限key)
     * @ibatorgenerated 2015-09-01 10:10:36
     */
    private String key;

    /**
     * permission.parent_id (上级权限)
     * @ibatorgenerated 2015-09-01 10:10:36
     */
    private Integer parentId;

    /**
     * permission.order (权限排序)
     * @ibatorgenerated 2015-09-01 10:10:36
     */
    private Integer order;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }
}