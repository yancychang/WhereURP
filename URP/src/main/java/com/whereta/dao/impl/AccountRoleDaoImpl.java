package com.whereta.dao.impl;

import com.whereta.dao.IAccountRoleDao;
import com.whereta.mapper.AccountRoleMapper;
import com.whereta.model.AccountRole;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.Set;

/**
 * @author Vincent
 * @time 2015/8/27 17:05
 */
@Repository("accountRoleDao")
public class AccountRoleDaoImpl implements IAccountRoleDao {
    @Resource
    private AccountRoleMapper accountRoleMapper;

    /**
     * 根据账号id获取角色id
     * @param accountId
     * @return
     */
    public Set<Integer> selectRoleIdSet(int accountId) {
        return accountRoleMapper.selectRoleIdSet(accountId);
    }

    /**
     * 创建账号角色
     * @param accountRole
     * @return
     */
    public int createAccountRole(AccountRole accountRole) {
        return accountRoleMapper.insertSelective(accountRole);
    }

    /**
     * 根据角色删除account-role
     * @param roleId
     * @return
     */
    public int deleteByRoleId(int roleId) {
        return accountRoleMapper.delete(roleId,null);
    }

    /**
     *   根据账号删除account-role
     * @param accountId
     * @return
     */
    public int deleteByAccountId(int accountId) {
        return accountRoleMapper.delete(null,accountId);
    }
}
